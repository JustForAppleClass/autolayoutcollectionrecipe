//
//  RecipeViewController.h
//  autoLaytoutRecipeApp
//
//  Created by Matthew Griffin on 6/11/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Recipe.h"

@interface RecipeViewController : UIViewController

@property(nonatomic, strong)Recipe* viewRecipe;
@property(nonatomic, strong)UILabel* lbl;
@property(nonatomic, strong)UITextView* ingredients;
@property(nonatomic, strong)UITextView* instructions;
@property(nonatomic, strong)UIImageView* picture;

@end
