//
//  ViewController.m
//  autoLaytoutRecipeApp
//
//  Created by Matthew Griffin on 6/11/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.recipe = [RecipeViewController new];
    
    CGSize sizer = self.view.frame.size;
    NSString* filepath = [[NSBundle mainBundle]pathForResource:@"RecipeList" ofType:@"plist"];
    NSArray* arrayofRecipes = [[NSArray alloc]initWithContentsOfFile:filepath];
    self.recipeArr =[[NSMutableArray alloc]init];
    for (int i =0; i < arrayofRecipes.count; i++) {
        NSDictionary* temp = arrayofRecipes[i];
        Recipe* oneRec = [Recipe new];
        oneRec.ingredients = temp[@"Ingredients"];
        oneRec.directions = temp[@"Instructions"];
        oneRec.image = temp[@"image"];
        oneRec.name = temp[@"name"];
        self.recipeArr[i] = oneRec;
    }
    [self.recipe viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UICollectionViewFlowLayout* vfl = [[UICollectionViewFlowLayout alloc]init];
    
    UICollectionView* cv = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, sizer.width, sizer.height) collectionViewLayout:vfl];
    cv.dataSource = self;
    cv.delegate = self;
    [cv registerClass:[MyCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    [self.view addSubview:cv];
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    MyCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath ];
    
    cell.backgroundColor = [UIColor greenColor];
    
    cell.lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/2 - 20, 20)];
    cell.lbl.backgroundColor = [UIColor orangeColor];
    Recipe* temp = self.recipeArr[indexPath.row];
    cell.lbl.text = temp.name;
    cell.picture = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
    [cell.picture setContentMode:UIViewContentModeScaleToFill];
    [cell.picture setImage: [UIImage imageNamed:temp.image]];
    [cell addSubview:cell.picture];
    [cell addSubview:cell.lbl];
    
    
    
    
    
    return cell;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.recipeArr.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width/2 - 20, 200);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self.navigationController pushViewController:self.recipe animated:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ViewRecipe" object:self.recipeArr[indexPath.row]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
