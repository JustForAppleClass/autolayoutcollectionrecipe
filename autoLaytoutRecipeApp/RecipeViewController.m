//
//  RecipeViewController.m
//  autoLaytoutRecipeApp
//
//  Created by Matthew Griffin on 6/11/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView* view = self.view;
    
    
    self.ingredients = [UITextView new];
    self.instructions = [UITextView new];
    self.picture = [UIImageView new];
    self.lbl = [UILabel new];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(NotificationRecieved:) name:@"ViewRecipe" object:nil];
    
    
    self.view.backgroundColor = [UIColor blackColor];
    
    //Add observer for notifications
    
    
    //Change a few background colors and otherwise prepare label, textviews, and image.
    self.lbl.backgroundColor = [UIColor purpleColor];
    [self.lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.lbl.textColor =[UIColor whiteColor];
    [self.ingredients setTranslatesAutoresizingMaskIntoConstraints: NO];
    [self.ingredients setEditable:NO];
    self.ingredients.backgroundColor = [UIColor purpleColor];
    self.ingredients.textColor = [UIColor whiteColor];
    
    
    [self.instructions setTranslatesAutoresizingMaskIntoConstraints: NO];
    self.instructions.backgroundColor = [UIColor orangeColor];
    self.instructions.textColor =[UIColor whiteColor];
    [self.instructions setEditable:NO];
    
    [self.picture setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.picture setContentMode:UIViewContentModeScaleAspectFit];
    
    self.lbl.backgroundColor = [UIColor orangeColor];
    
    [self.view addSubview:self.lbl];
    [self.view addSubview:self.ingredients];
    [self.view addSubview:self.instructions];
    [self.view addSubview:self.picture];
    
    
    
    
 
    NSDictionary *metrics = @{@"height":@50.0,
                              @"width":@300};
    NSDictionary *views = @{
                            @"one":self.lbl,
                            @"two":self.picture,
                            @"three":self.ingredients,
                            @"four":self.instructions,
                            };
    [self.view addSubview:self.lbl];
    [self.view addSubview:self.instructions];
    [self.view addSubview:self.ingredients];
    [self.view addSubview:self.picture];
    
    
    
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-==80-[one(<=30)]-[two(four)]-==30-[three(four)]-==10-[four]-|" options:NSLayoutFormatAlignAllRight |NSLayoutFormatAlignAllLeft metrics:metrics views:views];
    [view addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[one]-|" options:0 metrics:metrics views:views];
    [view addConstraints:constraints];
    
    
    
    
    
    
}

-(void)NotificationRecieved:(NSNotification*)n
{
    self.viewRecipe = n.object;
    self.lbl.text = self.viewRecipe.name;
    self.instructions.text = self.viewRecipe.directions;
    self.ingredients.text = self.viewRecipe.ingredients;
    
    self.picture.image = [UIImage imageNamed:self.viewRecipe.image];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
