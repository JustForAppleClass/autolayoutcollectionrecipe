//
//  MyCollectionViewCell.h
//  autoLaytoutRecipeApp
//
//  Created by Michelle Griffin on 6/11/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionViewCell : UICollectionViewCell

-(instancetype)init;

@property(nonatomic, strong)UILabel* lbl;

@property(nonatomic, strong)UIImageView* picture;

@end
