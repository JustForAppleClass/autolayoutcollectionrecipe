//
//  ViewController.h
//  autoLaytoutRecipeApp
//
//  Created by Matthew Griffin on 6/11/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipeViewController.h"
#import "MyCollectionViewCell.h"

@interface ViewController : UIViewController<UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource>

@property(nonatomic, strong)RecipeViewController* recipe;

@property(nonatomic, strong) NSMutableArray* recipeArr;

@end

