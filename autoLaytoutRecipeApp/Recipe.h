//
//  Recipe.h
//  autoLaytoutRecipeApp
//
//  Created by Michelle Griffin on 6/12/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Recipe : NSObject


@property(nonatomic, strong) NSString* name;

@property(nonatomic, strong) NSString* ingredients;
@property(nonatomic, strong) NSString* directions;
@property(nonatomic, strong) NSString* image;

-(instancetype)init;


@end
